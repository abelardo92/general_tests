import React from 'react';

const Message = () => {
    const [showParagraph, setShowParagraph] = React.useState(false)
    const handleClick = () => setShowParagraph(!showParagraph)
    return (
        <>
        <button onClick={handleClick}>Want to buy a new car?</button>
        {showParagraph && <p>Call +11 22 33 44 now!</p>}
        </>
    )
}
export default Message
