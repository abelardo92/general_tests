import React from 'react';

const Product = ({ index, product, onVote }) => {
    //const { index, product, onVote } = props
    const plus = () => {
        onVote(1, index)
      // Call props.onVote to increase the vote count for this product
    };
    const minus = () => {
        onVote(-1, index)
      // Call props.onVote to decrease the vote count for this product
    };
    return (
        <li>
            <span>{product.name}</span> - <span>votes: {product.votes}</span>
            <button onClick={plus}>+</button>{" "}
            <button onClick={minus}>-</button>
        </li>
    );
};

class GroceryApp extends React.Component {

    // Finish writing the GroceryApp class

    // IMPORTANT!!: In some cases testDome don't recognize the constructor, in that happens try THIS!!
    // state = { products: this.props.products };
    constructor(props) {
        super(props)
        // this.state = {
        //     products : this.props.products
        // }
        this.state = {
            products : [
                { name: "Oranges", votes: 0 },
                { name: "Apples", votes: 0 },
                { name: "Bananas", votes: 0 }
            ],
        }
    }

    onVote = (dir, index) => {
        console.log(dir)
        this.setState(
            this.state.products.map((product, i) => {
                if (i === index) return product.votes += dir;
                return product;
            })
        );
    };

    render() {
        const { products } = this.state;
        return (
            <ul>
            {products.map((product, index) => <Product key={index} index={index} product={product} onVote={this.onVote}/> )}
            {/* Render an array of products, which should call this.onVote when + or - is clicked */}
            </ul>
        );
    }
}

export default GroceryApp;
