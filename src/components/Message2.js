import React from 'react';

class Message2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showParagraph: false
        }
    }

    handleClick(e) {
        this.setState({showParagraph: !this.state.showParagraph});
    }

    render() {
        return (
            <>
            <button onClick={this.handleClick.bind(this)}>Want to buy a new car?</button>
            {this.state.showParagraph && <p>Call +11 22 33 44 now!</p>}
            </>
        )
    }
}
export default Message2