import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import FocusableInfo from './components/FocusableInput.js'
import Message from './components/Message.js'
import Message2 from './components/Message2.js'
import Test1 from './components/Test1.js'
import Test2 from './components/Test2.js'
import Test3 from './components/Test3.js'
import Test4 from './components/Test4.js'
import GroceryApp from './components/GroceryApp.js'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={FocusableInfo}/>
        <Route exact path="/message" component={Message}/>
        <Route exact path="/message2" component={Message2}/>
        <Route exact path="/groceryapp" component={GroceryApp}/>
        <Route exact path="/test1" component={Test1}/>
        <Route exact path="/test2" component={Test2}/>
        <Route exact path="/test3" component={Test3}/>
        <Route exact path="/test4" component={Test4}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
